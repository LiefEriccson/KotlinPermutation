Just a small repo that tried to explore the permutation challenge using Kotlin.

``` companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val str = "ABCDEFGHIJ"
            val n = str.length
            Permutation().permute(str, 0, n - 1)
        }
    }```
	
Here is the main method. It is currently on both kotlin files as I wanted to run them both individually.

`val str = `
Edit this value with whatver you want. The program will permutate through that string (Will move this to a constant later).


I probably did this whole gradle stuff wrong in terms of organization, but here is the source of the Kotlin files:
`/randomstuff/src/main/kotlin`


Average completion time for both files is roughly 12 seconds. 

However, I would like to say that the multithreaded version will see much better gains if the function call will take more than like 0.000001 seconds to complete.



Oh. If you don't wanna actually download this and deal with gradle and all that nonsense, [just click here](http://rextester.com/l/kotlin_online_compiler) for an online compiler.