package com.stuff.random

import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class PermutationWithThreading{
    val threadCount = Runtime.getRuntime().availableProcessors()
    val threadPool = Executors.newFixedThreadPool(threadCount)
    val scheduler = Schedulers.from(threadPool)

    fun permute(string: String, startingPosition: Int, endPosition: Int) {
        var str = string
        if (startingPosition == endPosition)
            println(str)
        else {
            for (i in startingPosition..endPosition) {
                str = swapCharacters(str, startingPosition, i)
                Observable.just(permute(str, startingPosition + 1, endPosition))
                        .subscribeOn(scheduler)
                //string = swapCharacters(string,startingPosition,i);
            }
        }
    }

    fun swapCharacters(string: String, firstPosition: Int, secondPosition: Int): String {
        val temp: Char
        val charArray = string.toCharArray()
        temp = charArray[firstPosition]
        charArray[firstPosition] = charArray[secondPosition]
        charArray[secondPosition] = temp
        return String(charArray)
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val str = "ABCDEFGHIJ"
            val n = str.length
            val preTime = System.currentTimeMillis()
            Permutation().permute(str, 0, n - 1)

            val postTime = System.currentTimeMillis()
            println("Total time to run was ${postTime-preTime} ms.")
        }
    }
}