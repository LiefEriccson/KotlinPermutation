package com.stuff.random

class Permutation {

    fun permute(string: String, startingPosition: Int, endPosition: Int) {
        var str = string
        if (startingPosition == endPosition)
            println(str)
        else {
            for (i in startingPosition..endPosition) {
                str = swapCharacters(str, startingPosition, i)
                permute(str, startingPosition + 1, endPosition)
                //string = swapCharacters(string,startingPosition,i);
            }
        }
    }

    fun swapCharacters(string: String, firstPosition: Int, secondPosition: Int): String {
        val temp: Char
        val charArray = string.toCharArray()
        temp = charArray[firstPosition]
        charArray[firstPosition] = charArray[secondPosition]
        charArray[secondPosition] = temp
        return String(charArray)
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val str = "ABCDEFGHIJ"
            val n = str.length
            Permutation().permute(str, 0, n - 1)
        }
    }
}
